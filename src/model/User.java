package model;

import org.apache.commons.codec.digest.DigestUtils;

import javax.persistence.*;

/**
 * A customer has a name and customer identity number
 * and a number of reward points.
 *
 * Privilege allows users to access the admin site to edit the database
 * 
 * @author Dan Miles
 */
@Entity
@Table(name = "users")
public class User {

	//fields
    @OneToOne(cascade = CascadeType.MERGE)
    @PrimaryKeyJoinColumn
	private Basket basket;

	@Column(name = "name")
	private String name;

	@Id
    @GeneratedValue
    @Column(name = "user_id")
	private int ID;

	@Column(name = "admin_priv")
	private boolean adminPrivilege;

	@Column(name = "reward_points")
	private int rewardPoints;

	@Column(name = "password")
	private String password;

    public boolean isAdminPrivilege() {
        return adminPrivilege;
    }

    public void setAdminPrivilege(boolean adminPrivilege) {
        this.adminPrivilege = adminPrivilege;
    }


	//constructors
	public User() {
		this.name = new String();
		this.rewardPoints = 0;
        this.basket = new Basket();
        this.adminPrivilege = false;
	}
	
	public User(String name, Basket basket) {
		this.name = name;
		this.rewardPoints = 0;
		this.basket = basket;
		this.adminPrivilege = false;
	}

	public User(String name){
	    this.name = name;
	    this.rewardPoints = 0;
	    this.basket = new Basket();
	    this.adminPrivilege = false;
    }

	
	//methods
    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

	public void setName(String name) {
		this.name = name;
	}

	public int getUserId() {
		return ID;
	}

	public void setUserId(int UserId) {
		this.ID = UserId;
	}
	
	public int getRewardPoints() {
		return rewardPoints;
	}
	
	public void setRewardPoints(int rewardPoints) {
		this.rewardPoints += rewardPoints;
	}

	public Basket getBasket(){
	    return basket;
    }

    public void setBasket(Basket basket) {
        this.basket = basket;
    }

    @Override
	public String toString() {
		return this.getClass().getSimpleName() + ":[name=" + name +
				", ID=" + ID + ", rewardPoints=" + rewardPoints + "]";
	}

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = DigestUtils.sha256Hex(password);
    }
}
