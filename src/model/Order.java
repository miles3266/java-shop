package model;


import javax.persistence.*;

/**
 * @author Dan Miles
 */

@Entity
@Table(name = "orders")
public class Order implements Comparable<Order> {

	//fields

    @Id
    private int ID;

    @OneToOne(cascade = CascadeType.MERGE)
    @PrimaryKeyJoinColumn
    private Product item;

    @Column(name = "quantity")
	private int quantity;

	
	//constructors
	public Order() {
		item = new Product();
		quantity = 0;
		
	}

	public Order(Product item, int quantity) {
		this.item = item;
		this.quantity = quantity;
	}

	
	//methods
	public void increaseQuantity() {
		quantity++;
	}

	public void decreaseQuantity() {
		quantity--;
	}

	public int getCost() {
		return quantity * item.getUnitPrice();
	}

	public Product getItem() {
		return item;
	}

    public void setItem(Product item) {
        this.item = item;
    }

	public int getQuantity() {
		return quantity;
	}

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
	public String toString() {
		return this.getClass().getSimpleName() + ":[item=" + item + ", quantity=" + quantity + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || this.getClass() != obj.getClass())
			return false;

		Order other = (Order) obj;

		return this.item.equals(other.item);
	}

	@Override
	public int compareTo(Order other) {
		return this.item.compareTo(other.item);
	}

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }
}
