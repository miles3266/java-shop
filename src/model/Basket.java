package model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
/**
 * @author Dan Miles
 */
@Entity
@Table(name = "basket")
public class Basket implements Iterable<Order> {

	//fields
    @OneToMany(fetch = FetchType.EAGER)
	private List<Order> contents;

    @Temporal(TemporalType.DATE)
	private java.util.Date deliveryDate;

	@Id
    @Column(name = "basket_id")
	private int ID;

	//constructors
	public Basket() {
		contents = new ArrayList<Order>();
		deliveryDate = new java.util.Date();
		ID = 0;
	}

	public Basket(Date deliveryDate, int ID) {
		contents = new ArrayList<Order>();
		this.deliveryDate = deliveryDate;
		this.ID = ID;
	}


    public java.util.Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(java.util.Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    //methods
	public void setContents(List<Order> contents) {
		this.contents = contents;
	}

	public List<Order> getContents() {
		return contents;
	}

	public void addOrder(Order o) {
		contents.add(o);
	}
	
	public void removeOrder(int i) {
		contents.remove(i);
	}
	
	public void clear() {
		contents.clear();
	}
	
	public Order getOrder(int i) {
		return contents.get(i);
	}
	
	public int numberOfOrders() {
		return contents.size();
	}
	
	public int getTotalCost() {
		int total = 0;
		for (Order o : contents) {
			total+= o.getCost();
		}
		return total;
	}
	
	public boolean isEmpty() {
		return contents.isEmpty();
	}
	
	public Order findOrder(String productCode) {
		for(Order o : contents) {
			if (o.getItem().getProductCode().equals(productCode)) {
				return o;
			}
		}
		return null;
	}
	
	public void sortOrders() {
		Collections.sort(contents);
	}
	
	public boolean containsOrder(Order o) {
		return contents.contains(o);
	}
	
	public boolean removeOrder(Order o) {
		return contents.remove(o);
	}

	public int getID() {
		return ID;
	}

	public void setID(int ID) {
		this.ID = ID;
	}
	
	@Override
	public String toString() {
		return this.getClass().getSimpleName() + ":[contents=" + contents + 
				", deliveryDate=" + deliveryDate +
				", ID=" + ID + "]";
	}
	
	@Override
	public Iterator<Order> iterator() {
		return contents.iterator();
	}
	
}
