package model;

import javax.persistence.Entity;
import java.util.HashSet;
import java.util.Set;

/**
 * A reward processor holds a set of products that are entitled
 * to receive reward points and can process a given cart to achieve this.
 * 
 * @author Dan Miles
 */
public class RewardProcessor {

	private Set<Product> products;
	
	public RewardProcessor() {
		products = new HashSet<Product>();
	}
	
	public boolean addProduct(Product p) {
		return products.add(p);
	}
	
	public int rewardPoints(Basket b) {
		return 0; //TODO re-fix this method
	}
	
	@Override
	public String toString() {
		return this.getClass().getSimpleName() + "[products=" + products + "]";
	}
	
}
