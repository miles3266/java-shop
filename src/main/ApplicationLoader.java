
package main;

import controller.ShopController;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import view.ShopLoginRootPane;

/* Loads the application and creates an instance of the model, view and controller */
public class ApplicationLoader extends Application {

	private ShopLoginRootPane view;
	
	public void init() {
		
		view = new ShopLoginRootPane();
		//JourneyReturn model = new JourneyReturn();
		
	}
	
	@Override
	public void start(Stage stage) throws Exception {
		stage.setTitle("Shop");
		stage.setScene(new Scene(view, 1200, 800));
		new ShopController(view, stage);
		stage.show();
	}

	public static void main(String[] args) {
//		ProductDaoService productDaoService = new ProductDaoService();
//		DaoService daoService = new DaoService();
//        User testUser = new User("Admin");
//        testUser.setPassword("letmein");
//        testUser.setAdminPrivilege(true);
//        Product testProduct = new Product("000-001", "This is a test", 100);
//        productDaoService.persist(testProduct);
//        daoService.persist(testUser);
		launch(args);
	}

}
