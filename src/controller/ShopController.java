
package controller;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.stage.Stage;
import model.User;
import view.*;

//import view.AdminLoginPane;

public class ShopController {
	//declare fields to be used throughout class
	private Stage stage;
	private ShopSelectionPane selectionPane;
	//private AdminLoginPane adminloginPane;
	private ShopLoginRootPane customerloginPane;
	private ShopAccountCreationPane accountcreationPane;
	private AccountCreationButtonsPane ac;
	private ShopAdminPane shopadminPane; 
	private ShopLoginRootPane view;
	private LoginMenuBarPane bp;
	private LoginButtonsPane np;

	private static String username;
	private static String password;
	private static Boolean admin;


	public ShopController(ShopLoginRootPane view,  Stage stage) {
		//initialise model and view
		//this.model = model;
		this.view = view;
		this.stage = stage;
        admin = false;
		//initialise view subcontainer fields for convenient access to these
		
		bp = view.getButtonPane();
		np = view.getNamePane();

		//attach event handlers to view using private helper method
		this.attachEventHandlers();
	}

	private void attachEventHandlers() {
		//attach button and menu item handlers to the view, which can either be done as inner-classes or lambda expressions
		//example of adding a change listener to a property in the view - the lambda uses the private helper method below
	
		np.addLoginHandler(new LoginHandler()); //Login button?
		np.addAccountHandler(new AccountHandler()); //The create account thingy
		//bp.addAdmingLoginHandler(new AdminLoginHandler());
		bp.addSelectionChangeListener((observable, oldValue, newValue) -> selectionChangeHandler(newValue)); //admin/customer drop down?
		
	}
	
	//A private helper method used for the selection change handler.
	//This is another approach when defining event handlers - to use a lambda when
	//attaching and then call a private method which defines the handler's logic.
	private class LoginHandler implements EventHandler<ActionEvent> {

		public void handle(ActionEvent e) {

		    username = np.getTxtUsername().getText();
		    password = np.getTxtPassword().getText();
            DaoService daoService = new DaoService();
            User user = daoService.findByUsername(username);

            if (user != null) {
                if (admin = false) {
                    if (password == user.getPassword()) {
                        selectionPane = new ShopSelectionPane();
                        Scene selectionScene = new Scene(selectionPane);
                        stage.setScene(selectionScene);
                        stage.show();
                    }
                } else if (admin = true) {
                    if (password == user.getPassword() && user.isAdminPrivilege()) {
                        shopadminPane = new ShopAdminPane();
                        Scene shopadminScene = new Scene(shopadminPane);
                        stage.setScene(shopadminScene);
                        stage.show();
                    }
                }
            }
        }
	}

	private void selectionChangeHandler(String newValue) {
		if (newValue.equals("Customer")) {
		    admin = false;
			
		} else if (newValue.equals("Admin")) {
		    admin = true;
		}
	}
	
	private class AccountHandler implements EventHandler<ActionEvent> {

		public void handle(ActionEvent e) {
			accountcreationPane = new ShopAccountCreationPane();
			Scene accountcreationScene = new Scene(accountcreationPane);
			stage.setScene(accountcreationScene);
			stage.show();
		}
	}
}
