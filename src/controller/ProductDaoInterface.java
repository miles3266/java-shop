package controller;

import model.Product;

import java.io.Serializable;
import java.util.List;

public interface ProductDaoInterface<T, Id extends Serializable> {

    public void persist(Product entity);

    public void update(Product entity);

    public Product findByProductCode(String productCode);

    public void delete(Product entity);

    public List<Product> findAll();

    public void deleteAll();

}
