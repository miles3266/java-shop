package controller;

import model.User;

import java.util.List;

public class DaoService {
    private static Dao dao;

    public DaoService(){
        dao = new Dao();
    }

    public void persist(User user){
        dao.openCurrentSessionWithTransaction();
        dao.persist(user);
        dao.closeCurrentSessionWithTransaction();
    }

    public void update(User user){
        dao.openCurrentSessionWithTransaction();
        dao.update(user);
        dao.closeCurrentSessionWithTransaction();
    }

    public User findById(int id){
        dao.openCurrentSession();
        User user = dao.findById(id);
        dao.closeCurrentSession();
        return user;
    }

    public void delete(int id){
        dao.openCurrentSessionWithTransaction();
        User user = findById(id);
        dao.delete(user);
        dao.closeCurrentSessionWithTransaction();
    }

    public List<User> findAll(){
        dao.openCurrentSession();
        List<User> users = dao.findAll();
        dao.closeCurrentSession();
        return users;
    }

    public void deleteAll(){
        dao.openCurrentSession();
        dao.deleteAll();
        dao.closeCurrentSession();
    }

    public User findByUsername(String username){
        dao.openCurrentSession();
        User user = findByUsername(username);
        dao.closeCurrentSession();
        return user;
    }

    public static Dao getDao() {
        return dao;
    }
}
