package controller;

import model.Product;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import java.util.List;


public class ProductDao implements ProductDaoInterface{

    private Session currentSession;
    private Transaction currentTransaction;

    public ProductDao(){
        openCurrentSession();
        closeCurrentSession();
    }

    public static SessionFactory getSessionFactory(){
        Configuration configuration = new Configuration();
        configuration.addAnnotatedClass(model.User.class);
        configuration.addAnnotatedClass(model.Basket.class);
        configuration.addAnnotatedClass(model.Order.class);
        configuration.addAnnotatedClass(Product.class);
        configuration.addAnnotatedClass(model.DiscountProduct.class);
        configuration.configure();
        try {
            StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
            SessionFactory sessionFactory = configuration.buildSessionFactory(builder.build());
            return sessionFactory;
        }
        catch (Throwable ex){
            throw new ExceptionInInitializerError((ex));
        }
    }

    public Session openCurrentSession(){
        currentSession = getSessionFactory().getCurrentSession();
        return currentSession;
    }

    public Session openCurrentSessionWithTransaction(){
        currentSession = getSessionFactory().openSession();
        currentTransaction = currentSession.beginTransaction();
        return currentSession;
    }

    public void closeCurrentSession(){
        currentSession.close();
    }

    public void closeCurrentSessionWithTransaction(){
        currentTransaction.commit();
        currentSession.close();
    }

    public Session getCurrentSession() {
        return currentSession;
    }

    public void setCurrentSession(Session session){
        this.currentSession = session;
    }

    public Transaction getCurrentTransaction() {
        return currentTransaction;
    }

    public void setCurrentTransaction(Transaction transaction){
        this.currentTransaction = transaction;
    }

    @Override
    public void persist(Product product) {
        getCurrentSession().save(product);
    }

    @Override
    public void update(Product product) {
        getCurrentSession().update(product);
    }

    @Override
    public Product findByProductCode(String productCode) {
        Product product = (Product) getCurrentSession().get(Product.class, productCode);
        return product;
    }

    @Override
    public void delete(Product product) {
        getCurrentSession().delete(product);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Product> findAll() {
        List<Product> products = (List<Product>) getCurrentSession().createQuery("from Product ").list();
        return products;
    }

    @Override
    public void deleteAll() {
        List<Product> products = findAll();
        for (Product product : products){
            delete(product);
        }
    }
}
