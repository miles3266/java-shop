package controller;
import model.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class Dao implements DaoInterface{

    private Session currentSession;
    private Transaction currentTransaction;

    public Dao(){
        openCurrentSession();
        closeCurrentSession();
    }

    public static SessionFactory getSessionFactory(){
        Configuration configuration = new Configuration();
        configuration.addAnnotatedClass(model.User.class);
        configuration.addAnnotatedClass(model.Basket.class);
        configuration.addAnnotatedClass(model.Order.class);
        configuration.addAnnotatedClass(model.Product.class);
        configuration.addAnnotatedClass(model.DiscountProduct.class);
        configuration.configure();
        try {
            StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
            SessionFactory sessionFactory = configuration.buildSessionFactory(builder.build());
            return sessionFactory;
        }
        catch (Throwable ex){
            throw new ExceptionInInitializerError((ex));
        }
    }

    public Session openCurrentSession(){
        currentSession = getSessionFactory().getCurrentSession();
        return currentSession;
    }

    public Session openCurrentSessionWithTransaction(){
        currentSession = getSessionFactory().openSession();
        currentTransaction = currentSession.beginTransaction();
        return currentSession;
    }

    public void closeCurrentSession(){
        currentSession.close();
    }

    public void closeCurrentSessionWithTransaction(){
        currentTransaction.commit();
        currentSession.close();
    }

    public Session getCurrentSession() {
        return currentSession;
    }

    public void setCurrentSession(Session session){
        this.currentSession = session;
    }

    public Transaction getCurrentTransaction() {
        return currentTransaction;
    }

    public void setCurrentTransaction(Transaction transaction){
        this.currentTransaction = transaction;
    }

    @Override
    public void persist(User user) {
        getCurrentSession().save(user);
    }

    @Override
    public void update(User user) {
        getCurrentSession().update(user);
    }

    @Override
    public User findById(int id) {
        User user = (User) getCurrentSession().get(User.class, id);
        return user;
    }

    @Override
    public void delete(User user) {
        getCurrentSession().delete(user);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<User> findAll() {
        List<User> users = (List<User>) getCurrentSession().createQuery("from User ").list();
        return users;
    }

    @Override
    public void deleteAll() {
        List<User> users = findAll();
        for (User user : users){
            delete(user);
        }
    }

    public User findbyUsername(String username){
        User user = (User) getCurrentSession().get(User.class, username);
        return user;
    }
}
