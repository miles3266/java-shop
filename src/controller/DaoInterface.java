package controller;

import model.User;

import java.io.Serializable;
import java.util.List;

public interface DaoInterface<T, Id extends Serializable> {

    public void persist(User entity);

    public void update(User entity);

    public User findById(int id);

    public void delete(User entity);

    public List<User> findAll();

    public void deleteAll();

}
