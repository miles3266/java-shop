package controller;

import model.Product;

import java.util.List;

public class ProductDaoService {
    private static ProductDao dao;

    public ProductDaoService(){
        dao = new ProductDao();
    }

    public void persist(Product product){
        dao.openCurrentSessionWithTransaction();
        dao.persist(product);
        dao.closeCurrentSessionWithTransaction();
    }

    public void update(Product product){
        dao.openCurrentSessionWithTransaction();
        dao.update(product);
        dao.closeCurrentSessionWithTransaction();
    }

    public Product findByProductCode(String productCode){
        dao.openCurrentSession();
        Product product = dao.findByProductCode(productCode);
        dao.closeCurrentSession();
        return product;
    }

    public void delete(String productCode){
        dao.openCurrentSessionWithTransaction();
        Product product = findByProductCode(productCode);
        dao.delete(product);
        dao.closeCurrentSessionWithTransaction();
    }

    public List<Product> findAll(){
        dao.openCurrentSession();
        List<Product> product = dao.findAll();
        dao.closeCurrentSession();
        return product;
    }

    public void deleteAll(){
        dao.openCurrentSession();
        dao.deleteAll();
        dao.closeCurrentSession();
    }

    public static ProductDao getDao() {
        return dao;
    }
}
