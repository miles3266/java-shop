
package view;

import view.LoginMenuBarPane;
import javafx.geometry.Pos;
import javafx.scene.layout.VBox;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import view.AccountCreationButtonsPane;

/* The root of the journey planner, which creates subcontainers
 * and provides relevant access to other parts of the application.
 */
public class ShopAccountCreationPane extends GridPane {

	private LoginMenuBarPane bp;
	private AccountCreationButtonsPane ac;

	public ShopAccountCreationPane() {
	
		//this.setStyle("-fx-background-color: #FF0000;");
		this.setPrefSize(1200, 800);
		bp = new LoginMenuBarPane();
		ac = new AccountCreationButtonsPane();

		//this.setStyle("-fx-background-color: #97A7D;");
		
		this.setAlignment(Pos.TOP_CENTER);

		HBox bar = new HBox(); //restore, submit
		
	    bar.setStyle("-fx-background-color: #C4C8CA;");
	    bar.setPrefSize(300, 51);
	    
	
		VBox rootContainer = new VBox(bar, ac);
		
		rootContainer.setPrefSize(600, 300);
		
		rootContainer.setBorder(new Border(new BorderStroke(Color.web("#C4C8CA"), BorderStrokeStyle.SOLID, null, new BorderWidths(2))));
		this.setVgap(170);
		this.add(rootContainer, 1, 1);
	}

	/* These methods provide a public interface for the root pane and allow
	 * each of the sub-containers to be accessed by the controller.
	 */
	public LoginMenuBarPane getButtonPane() {
		return bp;
	}
	
	public AccountCreationButtonsPane getAccountButtonsPane() {
		return ac;
	}
	

	
	
	
	//method to hide/show panes and change background colour
	public void displayPanes(String type) {
	
	}
	
	
}
