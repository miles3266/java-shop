
package view;

import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import controller.ShopController;
/* A button pane for the journey planner allowing
 * various options such as clear, submit, and restore. */
public class LoginMenuBarPane extends HBox {

	//declared for access throughout class (handlers are now attached via methods, so buttons need to be declared as fields)
	//private Button login, submit, restore;
	private ComboBox<String> choices;
	
	public LoginMenuBarPane() {
		this.setStyle("-fx-background-color: #C4C8CA;");	
		this.setPadding(new Insets(10, 10, 10, 10));
		this.setAlignment(Pos.CENTER_RIGHT);
		/*
		login = new Button("Login");
		login.setPrefSize(70, 30);
	    submit = new Button("PLACEHOLDER");
	    submit.setPrefSize(70, 30);
	    restore = new Button("PLACEHOLDER");
	    restore.setPrefSize(70, 30); */
	    
	    ObservableList<String> list = FXCollections.observableArrayList("Customer", "Admin");
	    choices = new ComboBox<String>(list);
	    choices.getSelectionModel().select(0);
	    choices.setPrefSize(120, 30);
	    choices.setStyle("-fx-background-color: #C4C8CA;");
	    /*
	    HBox clearBox = new HBox(login);
	    clearBox.setAlignment(Pos.CENTER_LEFT);
	    clearBox.setPadding(new Insets(0,0,0,0));*/
	    
	    HBox submitBox = new HBox(choices); //restore, submit
	    submitBox.setAlignment(Pos.CENTER_RIGHT);
	    submitBox.setSpacing(0);
	    
	    
	    
	    
		this.getChildren().addAll(submitBox); //clearbox
		
		
	}
	
	//get the selected choice
	public String getSelectedChoice() {
		return choices.getSelectionModel().getSelectedItem();
	}
	
	//these methods allow handlers to be externally attached to this view by the controller
	public void addLoginHandler(EventHandler<ActionEvent> handler) {
		//login.setOnAction(handler);
	}
	
	public void addAdmingLoginHandler(EventHandler<ActionEvent> handler) {
		//submit.setOnAction(handler);
		
		//.getSelectionModel().selectedItemProperty().addListener((new ChangeListner<String>);
		
		/*choices.valueProperty().addListener(new ChangeListner<String>(){
			@Override public void changed(ObservableValue value, String old, String new){
				
			}
		}*/
	}
	
	public void addRestoreHandler(EventHandler<ActionEvent> handler) {
		//restore.setOnAction(handler);
	}
	
	//this method allows a change listener to be externally attached to the selectedItemProperty
	public void addSelectionChangeListener(ChangeListener<String> listener) {
		choices.getSelectionModel().selectedItemProperty().addListener(listener);
		
		
	}
	
}
