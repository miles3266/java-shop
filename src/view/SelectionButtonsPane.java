package view;


import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

public class SelectionButtonsPane extends GridPane {

	//private ComboBox<String> cboTitle;
	private TextField txtQuantity;
	private Button add, btnRemove, checkout;
	
	

	public SelectionButtonsPane() {
		
		//ListView listView = new ListView();
		
		//listView.getItems().add("Item 1");
		//listView.getItems().add("Item 2");
		//listView.getItems().add("Item 3");
		
		DatePicker dpDate = new DatePicker();
		
		TableView tblProducts = new TableView();
		
		TableView table = new TableView();
		table.setEditable(true);
		TableColumn ProductCode = new TableColumn("ID");
		TableColumn Item = new TableColumn("Item");
		//TableColumn Description = new TableColumn("Description");
		TableColumn Quantity = new TableColumn("Quantity");
		TableColumn Price = new TableColumn("Price");
		        
		table.getColumns().addAll(ProductCode, Item, Quantity, Price);
		tblProducts.getColumns().addAll(ProductCode, Item, Price);
		
		//removes highligting
		
		//tblProducts.setStyle("-fx-focus-color: transparent;");
		//tblProducts.setStyle("-fx-faint-focus-color: transparent;");
		
		//table.setStyle("-fx-focus-color: transparent;");
		//table.setStyle("-fx-faint-focus-color: transparent;");
		
		//Description.setPrefWidth(150);
		table.setPrefSize(320, 500);
		tblProducts.setPrefSize(320, 500);
		//240
		
		//styling
		checkout = new Button("Checkout");
		add = new Button("Add To Basket");
		btnRemove = new Button("Remove Item");
		//add.setPrefSize(70, 30);
		
		this.setPadding(new Insets(20, 20, 20, 20));
		this.setVgap(15);
		this.setHgap(40);
		//this.setStyle("-fx-background-color: #97A7D;");
		//this.setBorder(new Border(new BorderStroke(Color.web("#DADADA"), BorderStrokeStyle.SOLID, null, new BorderWidths(1))));
		//this.setAlignment(Pos.TOP_LEFT);
/*
		ColumnConstraints column0 = new ColumnConstraints();
		column0.setHalignment(HPos.RIGHT);

		ColumnConstraints column1 = new ColumnConstraints();
		column1.setHgrow(Priority.ALWAYS);

		this.getColumnConstraints().addAll(column0, column1);
*/
		//create labels
		Label lblQuantity = new Label("Quantity");
		Label lblProducts = new Label(" Product Selection");
		
		Label lblCart = new Label("Shopping Cart");
		Label lblDelivery = new Label("Delivery Date");
		Label lblCost = new Label("�00.00");
		//Label lblSurname = new Label("Password");


		// setup text fields
		txtQuantity = new TextField();
		txtQuantity.setPrefSize(70, 30);
		//txtSurname = new TextField();
		
		HBox bar = new HBox(lblProducts); //restore, submit
		bar.setAlignment(Pos.CENTER);
		
	    bar.setStyle("-fx-background-color: #C4C8CA;");
	    //bar.setPrefSize(300, 51);
	    
	    HBox bar2 = new HBox(lblCart); //restore, submit
		bar2.setAlignment(Pos.CENTER);
		
	    bar2.setStyle("-fx-background-color: #C4C8CA;");
	    //bar2.setPrefSize(300, 110);
	   // bar.setAlignment(Pos.TOP_LEFT);
	    
	    
		
		VBox Container1 = new VBox( tblProducts, lblQuantity, txtQuantity, add);
		Container1.setSpacing(15);
		
		HBox Con3 = new HBox(lblCost, checkout);
		checkout.setAlignment(Pos.CENTER);
		lblCost.setAlignment(Pos.CENTER_LEFT);
		Con3.setSpacing(40);
		
		VBox Container2 = new VBox( table, btnRemove, lblDelivery, dpDate, Con3);
		Container2.setSpacing(15);
		
		
		
		Container1.setPadding(new Insets(30, 40, 30, 40));
		Container2.setPadding(new Insets(30, 40, 30, 40));
		//Container1.setBorder(new Border(new BorderStroke(Color.web("#C4C8CA"), BorderStrokeStyle.SOLID, null, new BorderWidths(2))));
		
		VBox Con = new VBox(bar, Container1);
		Con.setBorder(new Border(new BorderStroke(Color.web("#C4C8CA"), BorderStrokeStyle.SOLID, null, new BorderWidths(1))));
		
		VBox Con2 = new VBox(bar2, Container2);
		Con2.setBorder(new Border(new BorderStroke(Color.web("#C4C8CA"), BorderStrokeStyle.SOLID, null, new BorderWidths(1))));
		
		
		//add controls and labels to container
		//this.add(lblQuantity, 0, 9);
		//this.add(lblProducts, 0, 5);
		
		//this.add(tblProducts, 0, 6);
		//this.add(txtQuantity, 0, 10);
		//this.add(add, 0, 12);
		//this.add(checkout, 3, 12);
		//this.add(lblCart, 3, 5);
		//this.add(table, 3, 6 );
		//this.add(btnRemove, 3, 8);
		//this.add(lblDelivery, 5, 6);
		this.add(Con, 1, 2);
		this.setHgap(400);
		this.add(Con2, 2, 2);
		this.setHgap(117);
		
		//this.setAlignment(Pos.CENTER);
		
		//this.add(dpDate, 5, 7);
	}

}
