
package view;

import view.LoginMenuBarPane;
import javafx.geometry.Pos;
import javafx.scene.layout.VBox;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;


/* The root of the shop, which creates subcontainers
 * and provides relevant access to other parts of the application.
 */
public class ShopLoginRootPane extends GridPane {

	private LoginMenuBarPane bp;
	private LoginButtonsPane np;
	
	
	public ShopLoginRootPane() {
	
		this.setStyle("-fx-background-color: #;");
		
		this.setPrefSize(1200, 800);
		bp = new LoginMenuBarPane();
		np = new LoginButtonsPane();
	     
		
		
		
		
		VBox rootContainer = new VBox(bp, np);// <-INSERT WHAT TO CONTAIN
		//set styling for main container
		//rootContainer.setPadding(new Insets(10,10,10,10));
		rootContainer.setSpacing(20);
		this.setVgap(170);
		rootContainer.setPrefSize(600, 300);
		
		//this.setBorder(new Border(new BorderStroke(Color.web("#999966"), BorderStrokeStyle.SOLID, null, new BorderWidths(2))));
		this.setAlignment(Pos.TOP_CENTER);
		
		rootContainer.setBorder(new Border(new BorderStroke(Color.web("#C4C8CA"), BorderStrokeStyle.SOLID, null, new BorderWidths(2))));
		
		this.add(rootContainer, 1, 1);
	}

	/* These methods provide a public interface for the root pane and allow
	 * each of the sub-containers to be accessed by the controller.
	 */
	public LoginMenuBarPane getButtonPane() {
		return bp;
	}
	
	public LoginButtonsPane getNamePane() {
		return np;
	}
	
	


	
	
	
	//method to hide/show panes and change background colour
	public void displayPanes(String type) {
	
	}
	
	
}
