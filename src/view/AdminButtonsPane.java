package view;


import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

public class AdminButtonsPane extends GridPane {

	//private ComboBox<String> cboTitle;
	private TextField txtProductCode, txtProduct, txtPrice, txtDiscount, txtRewardPoints;
	private Button btnAdd, btnRemove, btnEdit;
	
	

	public AdminButtonsPane() {
		
TableView tblProducts = new TableView();
		
		TableView table = new TableView();
		table.setEditable(true);
		TableColumn ProductCode = new TableColumn("ID");
		TableColumn Item = new TableColumn("Item");
		//TableColumn Description = new TableColumn("Description");
		TableColumn Quantity = new TableColumn("Quantity");
		TableColumn Price = new TableColumn("Price");
		
		        

		tblProducts.getColumns().addAll(ProductCode, Item, Price);
		
		tblProducts.setPrefSize(320, 500);
		
		//removes highligting
		
		//tblProducts.setStyle("-fx-focus-color: transparent;");
		//tblProducts.setStyle("-fx-faint-focus-color: transparent;");
		
		//table.setStyle("-fx-focus-color: transparent;");
		//table.setStyle("-fx-faint-focus-color: transparent;");
		
		//Description.setPrefWidth(150);
		//240
		
		//styling
		btnAdd = new Button("Add Product");
		btnEdit = new Button("Edit Product");
		btnRemove = new Button("Remove Product");
		//add.setPrefSize(70, 30);
		btnAdd.setPrefSize(130,  30);
		btnEdit.setPrefSize(130,  30);
		btnRemove.setPrefSize(130,  30);
		
		
		this.setPadding(new Insets(20, 20, 20, 20));
		this.setVgap(15);
		this.setHgap(40);
		//this.setStyle("-fx-background-color: #97A7D;");
		//this.setBorder(new Border(new BorderStroke(Color.web("#DADADA"), BorderStrokeStyle.SOLID, null, new BorderWidths(1))));
		//this.setAlignment(Pos.TOP_LEFT);
/*
		ColumnConstraints column0 = new ColumnConstraints();
		column0.setHalignment(HPos.RIGHT);

		ColumnConstraints column1 = new ColumnConstraints();
		column1.setHgrow(Priority.ALWAYS);

		this.getColumnConstraints().addAll(column0, column1);
*/
		//create labels
		Label lblProductCode = new Label("Enter Product Code");
		Label lblAdmin = new Label(" Product Manager");
		Label lblProduct = new Label("Enter Product Name");
		Label lblPrice = new Label("Product Price (p)");
		Label lblCart = new Label("Product Offfering");
		Label lblDiscount = new Label("Add a Discount");
		Label lblRewardPoints = new Label("Reward Points");
		
		
		
		Label lblDelivery = new Label("Delivery Date");
		Label lblCost = new Label("�00.00");
		//Label lblSurname = new Label("Password");


		// setup text fields
		txtProductCode = new TextField();
		txtProductCode.setPrefSize(150, 30);
		
		txtProduct = new TextField();
		txtProduct.setPrefSize(150, 30);
		
		txtPrice = new TextField();
		txtPrice.setPrefSize(150, 30);
		
		txtDiscount = new TextField();
		txtDiscount.setPrefSize(150, 30);
		
		txtRewardPoints = new TextField();
		txtRewardPoints.setPrefSize(150, 30);
		//txtSurname = new TextField();
		
		HBox bar = new HBox(lblAdmin); //restore, submit
		bar.setAlignment(Pos.CENTER);
		
	    bar.setStyle("-fx-background-color: #C4C8CA;");
	    //bar.setPrefSize(300, 51);
	    
	    HBox bar2 = new HBox(lblCart); //restore, submit
		bar2.setAlignment(Pos.CENTER);
		
	    bar2.setStyle("-fx-background-color: #C4C8CA;");
	    //bar2.setPrefSize(300, 110);
	   // bar.setAlignment(Pos.TOP_LEFT);
	    
	    HBox ProductCodeBox = new HBox(lblProductCode, txtProductCode); 
	    HBox ProductBox = new HBox(lblProduct, txtProduct);
	    HBox PriceBox = new HBox(lblPrice, txtPrice);
	    HBox DiscountBox = new HBox(lblDiscount, txtDiscount);
	    HBox RewardPointsBox = new HBox(lblRewardPoints, txtRewardPoints);
	    
	    ProductCodeBox.setSpacing(15);
	    ProductBox.setSpacing(9);
	    PriceBox.setSpacing(33);
	    DiscountBox.setSpacing(40);
	    RewardPointsBox.setSpacing(47);
		
		VBox Container1 = new VBox( ProductCodeBox, ProductBox, PriceBox, DiscountBox, RewardPointsBox, btnAdd, btnEdit, btnRemove);
		Container1.setSpacing(15);
		/*
		HBox Con3 = new HBox(lblCost, checkout);
		checkout.setAlignment(Pos.CENTER);
		lblCost.setAlignment(Pos.CENTER_LEFT);
		Con3.setSpacing(40);
		*/
		VBox Container2 = new VBox( tblProducts);
		Container2.setSpacing(15);
		
		
		
		Container1.setPadding(new Insets(30, 40, 30, 40));
		Container2.setPadding(new Insets(30, 40, 30, 40));
		//Container1.setBorder(new Border(new BorderStroke(Color.web("#C4C8CA"), BorderStrokeStyle.SOLID, null, new BorderWidths(2))));
		
		VBox Con = new VBox(bar, Container1);
		Con.setBorder(new Border(new BorderStroke(Color.web("#C4C8CA"), BorderStrokeStyle.SOLID, null, new BorderWidths(1))));
		
		VBox Con2 = new VBox(bar2, Container2);
		Con2.setBorder(new Border(new BorderStroke(Color.web("#C4C8CA"), BorderStrokeStyle.SOLID, null, new BorderWidths(1))));
		
		
		//add controls and labels to container
		//this.add(lblQuantity, 0, 9);
		//this.add(lblProducts, 0, 5);
		
		//this.add(tblProducts, 0, 6);
		//this.add(txtQuantity, 0, 10);
		//this.add(add, 0, 12);
		//this.add(checkout, 3, 12);
		//this.add(lblCart, 3, 5);
		//this.add(table, 3, 6 );
		//this.add(btnRemove, 3, 8);
		//this.add(lblDelivery, 5, 6);
		this.add(Con, 1, 2);
		this.setHgap(400);
		this.add(Con2, 2, 2);
		this.setHgap(117);
		
		//this.setAlignment(Pos.CENTER);
		
		//this.add(dpDate, 5, 7);
	}

}
