package view;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;

public class AccountCreationButtonsPane extends GridPane {

	//private ComboBox<String> cboTitle;
	private LoginMenuBarPane bp;
	private LoginButtonsPane np;
	private TextField txtfirstName, txtfamilyName, txtPassword;
	private Button btnCreate;
	private Hyperlink lnkLogin;
	

	public AccountCreationButtonsPane() {
		
		btnCreate = new Button("Create Account");
		btnCreate.setPrefSize(140, 30);
		//styling
		txtfirstName = new TextField();
		txtfamilyName = new TextField();
		
		lnkLogin = new Hyperlink("Back to Login Page!");
		
		HBox optionsBox = new HBox(btnCreate, lnkLogin);
		optionsBox.setSpacing(100);
		//this.setVgap(170);
		//this.setHgap(20);
		
		PasswordField txtPassword = new PasswordField();
		
		Label lblFirstName = new Label("First Name");
		Label lblSurname = new Label("Surname");
		Label lblPassword = new Label("Password");
		
		this.setPadding(new Insets(20, 20, 20, 20));
		this.setVgap(15);
		this.setHgap(20);
		//this.setStyle("-fx-background-color: #97A7D;");
		
		this.setAlignment(Pos.CENTER);
		//Hyperlink link = new Hyperlink();
		//link.setText("Click here");
		//TextFlow flow = new TextFlow(new Text("                     Don't have an account? "), new Hyperlink("Click here"));
		
		ColumnConstraints column0 = new ColumnConstraints();
		column0.setHalignment(HPos.RIGHT);

		ColumnConstraints column1 = new ColumnConstraints();
		column1.setHgrow(Priority.ALWAYS);

		this.getColumnConstraints().addAll(column0, column1);
		
		
		

		// setup text fields
		txtfirstName = new TextField();
		txtfamilyName = new TextField();

		//add controls and labels to container
		//this.add(lblTitle, 0, 0);
		//this.add(cboTitle, 1, 0);
		//this.add(bar, 0, 0);
		this.add(lblFirstName, 0, 1);
		this.add(txtfirstName, 1, 1);
		this.add(txtfamilyName, 1, 2);
		this.add(lblSurname, 0, 2);
		this.add(lblPassword, 0,  3);
		this.add(txtPassword, 1, 3);
		this.add(optionsBox, 1, 4);
		
		
		//this.setVgap(100);
		
		//this.add(linkBox, 1, 4);
		
	
		
	}
	
	
	public void addLoginHandler(EventHandler<ActionEvent> handler) {
		lnkLogin.setOnAction(handler);
	}
	
	
	
	
	
	
	/*
	//clear the input fields
	public void clear() {
		cboTitle.getSelectionModel().selectFirst();
		txtFirstName.setText("");
		txtSurname.setText("");
	}

	//returns a name object based on the input fields
	public Name getNameInput() {
		String title = cboTitle.getSelectionModel().getSelectedItem();
		String fName =  txtFirstName.getText();
		String lName = txtSurname.getText();
		return new Name(title, fName, lName);
	} */

}
