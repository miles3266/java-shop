
package view;

import view.LoginMenuBarPane;
import view.SelectionButtonsPane;
import view.AdminButtonsPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.geometry.Pos;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;


/* The root of the shop, which creates subcontainers
 * and provides relevant access to other parts of the application.
 */
public class ShopAdminPane extends VBox {

	private LoginMenuBarPane bp;
	private SelectionButtonsPane pp;
	private AdminButtonsPane ap;

//	GridPane gridPane = new GridPane();
	
	public ShopAdminPane() {
	
		//this.setStyle("-fx-background-color: #;");  <---- CHANGE BACKGROUND COLOUR
		this.setPrefSize(1200, 800);
		bp = new LoginMenuBarPane();
		pp = new SelectionButtonsPane();
		ap = new AdminButtonsPane();
		
		//ap.setAlignment(Pos.TOP_LEFT);
		//pp.setAlignment(Pos.TOP_RIGHT);
		
		HBox box = new HBox();
		
		this.getChildren().addAll(ap);   //<----- MAIN ADD
	}

	/* These methods provide a public interface for the root pane and allow
	 * each of the sub-containers to be accessed by the controller.
	 */
	public LoginMenuBarPane getButtonPane() {
		return bp;
	}
	

	
	
	
	//method to hide/show panes and change background colour
	public void displayPanes(String type) {
	
	}
	
	
}
