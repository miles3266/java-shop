package view;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;


public class LoginButtonsPane extends GridPane {

	//private ComboBox<String> cboTitle;
	private TextField txtUsername;
	private Button btnLogin;
	private Hyperlink lnkAccount;
	private PasswordField txtPassword;


	public LoginButtonsPane() {
		//styling
		lnkAccount = new Hyperlink("Don't have an account? Click here!");
		
		VBox linkBox = new VBox(lnkAccount);
		
		PasswordField txtPassword = new PasswordField();
		//passwordField.setPromptText("Your password");
		
	    
		
		btnLogin = new Button("Login");
		btnLogin.setPrefSize(90, 30);
		
		this.setPadding(new Insets(20, 20, 20, 20));
		this.setVgap(15);
		this.setHgap(20);
		//this.setStyle("-fx-background-color: #97A7D;");
		
		this.setAlignment(Pos.CENTER);
		
		ColumnConstraints column0 = new ColumnConstraints();
		column0.setHalignment(HPos.RIGHT);

		ColumnConstraints column1 = new ColumnConstraints();
		column1.setHgrow(Priority.ALWAYS);

		this.getColumnConstraints().addAll(column0, column1);

		//create labels
		//Label lblTitle = new Label("Title");
		Label lblUsername = new Label("Username");
		Label lblPassword = new Label("Password");

		

		// setup text fields
		txtUsername = new TextField();
		

		//adding to pane
		this.add(lblUsername, 0, 1);
		this.add(txtUsername, 1, 1);
		this.add(txtPassword, 1, 2);
		this.add(lblPassword, 0, 2);
		this.add(btnLogin, 1, 3);	
		this.add(linkBox, 1, 4);
		
		lnkAccount.setOnAction((event) -> {
			System.out.println("test");	
		});
		
	}
	
	public void addLoginHandler(EventHandler<ActionEvent> handler) {
		btnLogin.setOnAction(handler);
	}
	
	public void addAccountHandler(EventHandler<ActionEvent> handler) {
		lnkAccount.setOnAction(handler);
	}

    public PasswordField getTxtPassword() {
        return txtPassword;
    }

    public TextField getTxtUsername() {
        return txtUsername;
    }
}
